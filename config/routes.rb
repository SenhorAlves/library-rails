Rails.application.routes.draw do
  get 'home/index'
  root "home#index"
  
  resources :books
  resources :clients
  resources :librarians
  resources :authors
  resources :reservations
end
