class AuthorsController < ApplicationController
    before_action :set_author, only: [:edit, :show, :update, :destroy]
    add_flash_types :red, :green, :yellow

    def index
        @authors = Author.all
    end

    def show
    end

    def new
        @author = Author.new
    end

    def create
        @author = Author.new(author_params)
        
        if @author.save
            redirect_to authors_path, green: ["Author criado com sucesso"]
        else
            redirect_to new_author_path, red: @author.errors.full_messages
        end
    end

    def edit
    end

    def update
        if @author.update(author_params)
            redirect_to authors_path, green: ["Author atualizado com sucesso"]
        else
            redirect_to edit_author_path(@author), red: @author.errors.full_messages
        end
    end

    def destroy
        @author.destroy
        redirect_to authors_path, green: ["Author apagado com sucesso"]
    end

    def set_author
        @author = Author.find(params[:id])
    end

    def author_params
        params.require(:author).permit(:name)
    end

end
