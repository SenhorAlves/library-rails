class ClientsController < ApplicationController
    before_action :set_client, only: [:edit, :show, :update, :destroy]
    add_flash_types :red, :green, :yellow

    def index
        @clients = Client.all
    end

    def show
    end

    def new
        @client = Client.new
    end

    def create
        @client = Client.new(client_params)
        
        if @client.save()
            redirect_to clients_path, green: ["Book criado com sucesso"]
        else
            redirect_to new_client_path, red: @client.errors.full_messages
        end
    end

    def edit
    end

    def update
        if @client.update(client_params)
            redirect_to clients_path, green: ["Client atualizado com sucesso"]
        else
            redirect_to edit_client_path(@client), red: @client.errors.full_messages
        end
    end

    def destroy
        @client.destroy
        redirect_to clients_path, green: ["Client apagado com sucesso"]
    end

    def set_client
        @client = Client.find(params[:id])
    end

    def client_params
        params.require(:client).permit(:name)
    end
end
