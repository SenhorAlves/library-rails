class LibrariansController < ApplicationController
    before_action :set_librarian, only: [:edit, :show, :update, :destroy]
    add_flash_types :red, :green, :yellow

    def index
        @librarians = Librarian.all
    end

    def show
    end

    def new
        @librarian = Librarian.new
    end

    def create
        @librarian = Librarian.new(librarian_params)
        
        if @librarian.save
            redirect_to librarians_path, green: ["Librarian criado com sucesso"]
        else
            redirect_to new_librarian_path, red: @librarian.errors.full_messages
        end
    end

    def edit
    end

    def update
        if @librarian.update(librarian_params)
            redirect_to librarians_path, green: ["Librarian atualizado com sucesso"]
        else
            redirect_to edit_librarian_path(@librarian), red: @librarian.errors.full_messages
        end
    end

    def destroy
        @librarian.destroy
        redirect_to librarians_path, green: ["Librarian deletado com sucesso"]
    end

    def set_librarian
        @librarian = Librarian.find(params[:id])
    end

    def librarian_params
        params.require(:librarian).permit(:name)
    end
end
