class ReservationsController < ApplicationController
    before_action :set_reservation, only: [:edit, :show, :update, :destroy]
    add_flash_types :red, :green, :yellow

    def index
        @reservations = Reservation.all
    end

    def show
    end

    def new
        @reservation = Reservation.new

        @books = Book.all
        @librarians = Librarian.all
        @clients = Client.all
    end

    def create
        @reservation = Reservation.new(reservation_params)
        
        if @reservation.save()
            redirect_to reservations_path, green: ["Reservation criado com sucesso"]
        else
            redirect_to new_reservation_path, red: @reservation.errors.full_messages
        end
    end

    def edit
        @books = Book.all
        @librarians = Librarian.all
        @clients = Client.all
    end

    def update
        if @reservation.update(reservation_params)
            redirect_to reservations_path, green: ["Reservation atualizado com sucesso"]
        else
            redirect_to edit_reservation_path(@reservation), red: @reservation.errors.full_messages
        end
    end

    def destroy
        @reservation.destroy
        redirect_to reservations_path, green: ["Reservation apagado com sucesso"]
    end

    def set_reservation
        @reservation = Reservation.find(params[:id])
    end

    def reservation_params
        params.require(:reservation).permit(:active, :client_id, :book_id, :librarian_id)
    end
end
