class BooksController < ApplicationController
    before_action :set_book, only: [:edit, :show, :update, :destroy]
    add_flash_types :red, :green, :yellow

    def index
        @books = Book.all
    end

    def show
    end

    def new
        @book = Book.new
        @authors = Author.all
    end

    def create
        @book = Book.new(book_params)
        
        if @book.save
            redirect_to books_path, green: ["Book criado com sucesso"]
        else
            redirect_to new_book_path, red: @book.errors.full_messages
        end
    end

    def edit
        @authors = Author.all
    end

    def update
        if @book.update(book_params)
            redirect_to books_path, green: ["Book atualizado com sucesso"]
        else
            redirect_to edit_book_path(@librarian), red: @librarian.errors.full_messages
        end
    end

    def destroy
        @book.destroy
        redirect_to books_path, green: ["Book apagado com sucesso"]
    end

    def set_book
        @book = Book.find(params[:id])
    end

    def book_params
        params.require(:book).permit(:name, :author_id)
    end
end
