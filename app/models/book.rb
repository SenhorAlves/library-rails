class Book < ApplicationRecord
  belongs_to :author
  has_many :reservations, dependent: :destroy

  validates :author_id, :name, presence: true
end
